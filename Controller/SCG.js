const googleMapsClient = require('@google/maps').createClient({
    key: 'AIzaSyAsfoTulroFx4UDOd5mu632oZpKYaWxY7E',
    Promise: Promise
})
const request = require('request')
const token = '+l0b6AJ7dU0qiX/JzeknpMroqiJr2xKM+92LtjOILrn9jeFXAiyncXiwTI6ufVib3gIf/hKDy+qOTdy1Z4LlmhpkVlp9ZJixK1RZCNT6dNp8ylbgEHNnSy0vMRhRNaf9lSlz8P+9M9DNvsKH70ErQwdB04t89/1O/w1cDnyilFU='

const SCG = {
    FindRestaurant(req){
        const { lat, lng, radius, pagetoken } = req.body;
        return new Promise(async (resolve, reject) => {
            if(pagetoken == null){
                googleMapsClient.placesNearby({
                    location: {
                        lat: lat,
                        lng: lng
                    },
                    radius: radius,
                    type: 'restaurant',
                })
                .asPromise()
                .then((response) => {
                    resolve([response.json.results,response.json.next_page_token]);
                })
            }else{
                googleMapsClient.placesNearby({
                    pagetoken: pagetoken
                })
                .asPromise()
                .then((response) => {
                    resolve([response.json.results,response.json.next_page_token]);
                })
            }
        })
    },
    Reply(reply_token, message){

        let headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer {${token}}`
        }

        if(message === 'image' || message === 'Image'){
            var body = JSON.stringify({
                replyToken: reply_token,
                messages: [{
                    type: 'image',
                    originalContentUrl: 'https://kaohoon.com/wp-content/uploads/2018/08/scg.jpg',
                    previewImageUrl: 'https://kaohoon.com/wp-content/uploads/2018/08/scg.jpg'
                }]
            })
        }
        // else if(message === 'video'){
        //     var body = JSON.stringify({
        //         replyToken: reply_token,
        //         messages: [{
        //             type: 'video',
        //             originalContentUrl: 'https://www.youtube.com/watch?v=Q8Ae1JlYu_k',
        //             previewImageUrl: 'https://kaohoon.com/wp-content/uploads/2018/08/scg.jpg'
        //         }]
        //     })
        // }
        else if(message === 'location' || message === 'Location'){
            var body = JSON.stringify({
                replyToken: reply_token,
                messages: [{
                    type: 'location',
                    title: 'SCG สำนักงานใหญ่',
                    address: '1 ซอย ปูนซีเมนต์ไทย แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800',
                    latitude: 13.806212,
                    longitude: 100.537707
                }]
            })
        }
        else{
            var body = JSON.stringify({
                replyToken: reply_token,
                messages: [{
                    type: 'text',
                    text: message
                }]
            })
        }
        
        request.post({
            url: 'https://api.line.me/v2/bot/message/reply',
            headers: headers,
            body: body
        },(err,res,body) => {
            console.log('status = ' + res.statusCode);
        })
    },


    FindSolution(req){
        const { num } = req.body;
        return new Promise(async (resolve,reject) => {          
            var array = [];
            for(i = 1; i<=num; i++){
                var result = (Math.pow(i,2) + 2) - (i - 1);
                array.push(result);
            }
            resolve(array);
        })
    }


}

module.exports = {
    SCG
}