const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = process.env.PORT || 3000
const {SCG} = require('./Controller/SCG')

const AIMLParser = require('aimlparser')
const aimlParser = new AIMLParser({ name: 'BotMe' })
aimlParser.load(['./test-aiml.xml'])

app.use(bodyParser.json())

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.listen(port, () => {
    console.log('Start server at port ' + port)
})

app.get('/', (req, res) => {
    res.send('Hello World');
})

app.post('/find-restaurant', async (req, res) => {
    const result = await SCG.FindRestaurant(req);
    res.send(result);
})

app.post('/webhook', async (req,res) => {
    const reply_token = req.body.events[0].replyToken
    const message = req.body.events[0].message.text
    await SCG.Reply(reply_token,message);
        
    /* await aimlParser.getResult(message, async (answer, wildCardArray, input) => {
         console.log('answer is : ',answer);
         res.send(answer);
    })*/
})

app.post('/find-solution', async (req, res) => {
    const result = await SCG.FindSolution(req);
    res.send(result);
})